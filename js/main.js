$(window).on("load", function() {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);

    var inscription = urlParams.get('inscription');

    if((inscription !== null) && inscription.length > 0){
        $("#token").val(inscription);
        $("#validation").modal('show');
    }

    if($.cookie("premierfois") === undefined){
        $.cookie("premierfois", Date.now());
        $("#information").modal('show');
    }

    //Charts
    var pour=$('#pour').text()
    var contre=$('#contre').text()

    var ctx = $('#myChart');
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: ['Pour '+ pour, 'Contre '+contre],
            datasets: [{
                data: [pour,contre],
                backgroundColor: [
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 99, 132, 0.2)'
                ],
                borderColor: [
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 99, 132, 1)'
                ]
            }]
        }
    });
    myChart.canvas.parentNode.style.height = '350px';
    myChart.canvas.parentNode.style.width = '300px';
});

$("body").on("click","#info", function(){
    $("#information").modal('show');
})

$(".vote").submit(function(e) {
    e.preventDefault();
    var form = $(this);
    console.log(form.serialize);
    var url = 'controller/vote.php';
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function(data){
            console.log(data);
            alert("Merci de votre vote !");
            document.location.reload();
        },
        error: function (data) {
            $('#act').val('tokenvote');
            $('#InscriptionLabel').text('Verification email');
            $('#inscription').modal('show');
        }
    });
});

$("body").on('click','#inscriptionlink',function(){
    $('#act').val('inscription');
    $('#InscriptionLabel').text('Inscription');
    $('#inscription').modal('show');
})


