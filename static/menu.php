<?php if (array_key_exists('authenticated', $_SESSION) && $_SESSION['authenticated'] === true) { }else{ } ?>

<nav class="nav nav-masthead justify-content-center float-md-end">
    <a class="nav-link active" href="#">vote</a>
    <?php 
        if (array_key_exists('authenticated', $_SESSION) && $_SESSION['authenticated'] === true) {
            echo '<a class="nav-link" href="controller/deauthentification.php">deconnexion</a>';
        }else{
            echo '<a id="inscriptionlink" class="nav-link" data-bs-toggle="modal" data-bs-target="#inscription">inscription</a>';
            echo '<a class="nav-link" href="#" data-bs-toggle="modal" data-bs-target="#authentification">connexion  </a>';
        } 
    ?>      
    
    <a class="nav-link" id="info" href="#">info</a>
</nav>