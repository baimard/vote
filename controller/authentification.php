<?php
    session_start();
    require "../class/bdd.class.php";
    require "../class/user.class.php";
    // var_dump($_POST);
    // echo $_GET['tokenvote'];
    if(isset($_POST['email']) && isset($_POST['password']) && USER::auth($_POST['email'], $_POST['password'])){
        $myUser = USER::getUser($_POST['email']);
        $_SESSION['email']=$myUser->getEmail();
        $_SESSION['prenom']=$myUser->getPrenom();
        $_SESSION['authenticated']=true;
        $_SESSION["login_time_stamp"]=time();
        http_response_code(200);
    }elseif(isset($_GET['tokenvote']) && USER::authToken($_GET['tokenvote'])){
        $myUser = USER::getUserToken($_GET['tokenvote']);
        USER::delTmpHash($_GET['tokenvote']);
        $_SESSION['email']=$myUser->getEmail();
        $_SESSION['prenom']=$myUser->getPrenom();
        $_SESSION['authenticated']=true;
        $_SESSION["login_time_stamp"]=time();
        header("Location: https://www.jeveuxvoter.fr"); 
    }else{
        http_response_code(401);
    }
?>