<?php
    session_start();
    require "../class/bdd.class.php";
    require "../class/user.class.php";
    require "../class/smtp.class.php";
    // var_dump($_POST);
    if(isset($_POST['email']) && USER::insertUser($_POST['email'],$_POST['prenom'])){
        $user = USER::getUser($_POST['email']);
        $newmail = New Smtp();
        if(isset($_POST['act']) && $_POST['act'] === "inscription"){
            $newmail->sendmail($user->getEmail(),"Votre inscription","Lien pour valider votre inscription https://www.jeveuxvoter.fr/?inscription=".$user->getTmphash());
        }elseif(isset($_POST['act']) && $_POST['act'] === "tokenvote"){
            $newmail->sendmail($user->getEmail(),"Votre inscription","Lien pour voter https://www.jeveuxvoter.fr/controller/authentification.php/?tokenvote=".$user->getTmphash());
        }
        
        
        http_response_code(200);
    }else{
        http_response_code(401);
    }
?>