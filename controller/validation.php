<?php
    session_start();
    require "../class/bdd.class.php";
    require "../class/user.class.php";
    require "../class/smtp.class.php";
    
    if(isset($_POST['token']) && isset($_POST['password']) && $_POST['password']!== "" && USER::setPassword($_POST['token'],$_POST['password'])){
        http_response_code(200);
    }else{
        http_response_code(401);
    }
?>