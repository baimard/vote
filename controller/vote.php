<?php
    session_start();
    require "../class/bdd.class.php";
    require "../class/user.class.php";

    if (array_key_exists('authenticated', $_SESSION) 
    && isset($_POST['question'])
    && $_SESSION['authenticated'] === true) {
        $user = USER::getUser($_SESSION['email']);
        if($_POST['action'] === "pour"){
            $user->votePour($_POST['question']);
        }elseif($_POST['action'] === "contre"){
            $user->voteContre($_POST['question']);
        }
        http_response_code(200);
    }else{
        http_response_code(401);
    }
?>