<?php
require 'mail.class.php';

class Smtp
{
    private $from;
    private $host;
    private $username;
    private $password;
    private $port;
    private $smtp;

    public function __construct(){
        $this->from = getenv('MAIL_FROM');
        $this->host = getenv('MAIL_HOST');
        $this->username = getenv('MAIL_USERNAME');
        $this->password=  getenv('MAIL_PASSWORD');
        $this->port=  getenv('MAIL_PORT');
        $this->smtp = Mail::factory('smtp',array(
            'host'=> $this->host,
            'auth'=> true,
            'username'=> $this->username,
            'password'=> $this->password,
            'port'=> $this->port,
        ));
    }

    /**
     * 
     * @to
     * @subject
     * @message
     */
    public function sendmail($to,$subject,$message){
        $recipients = $to;
        $headers['From']= $this->from;
        $headers['To']= $to;
        $headers['Subject'] = $subject;
        $headers["Content-Type"] = 'text/plain; charset="utf-8";';
        $headers["MIME-Version"] = '1.0';
        $body = $message;
        return $this->smtp->send($recipients,$headers,$body);
    }

}
