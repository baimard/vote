<?php

class Question{
    private $id;
    private $question;
    private $Pour;
    private $Contre;

    public function __construct(){}

    public function getLaquestion(){
        return $this->question;
    }

    public function getPour(){
        return $this->Pour;
    }

    public function getContre(){
        return $this->Contre;
    }

    public static function getQuestion($id){
        $BDD = BDD::getInstance();
        $auth = $BDD->prepare("SELECT * FROM `question` WHERE id = ?");
        $auth->execute(array($id));
        $auth->setFetchMode(PDO::FETCH_CLASS, 'Question');
        $res=$auth->fetchall();

        return $res[0];
    }
   
}