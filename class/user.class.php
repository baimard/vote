<?php

class User{
    private $id;
    private $email;
    private $prenom;
    private $password;
    private $tmphash;

    public function __construct(){}

    public function getEmail(){
        return $this->email;
    }

    public function getPrenom(){
        return $this->prenom;
    }

    public function getPassword(){
        return $this->password;
    }

    public function getTmphash(){
        return $this->tmphash;
    }

    public function avote(){
        $BDD = BDD::getInstance();
        $exist = $BDD->prepare("SELECT count(*) as c FROM `avote` WHERE user_id = ?");
        $exist->execute(array($this->id));
        $res=$exist->fetchall();

        if($res[0]['c'] > 0){
            return true;
        }else{
            return false;
        }
    }

    public function vote($question){
        $BDD = BDD::getInstance();
        $insert = $BDD->prepare("INSERT INTO `avote` (`id`, `user_id`, `question_id`) VALUES (NULL, ?, ?);");
        $insert->execute(array($this->id, $question));
        return true;
    }

    public function votePour($question){
        if(!$this->avote() && $this->vote($question)){
            $BDD = BDD::getInstance();
            $update = $BDD->prepare("UPDATE `question` SET `pour` = pour + 1 WHERE id = ?;");
            $update->execute(array($question));
            return true;
        }else{
            return false;
        }
    }

    public function voteContre($question){
        if(!$this->avote() && $this->vote($question)){
            $BDD = BDD::getInstance();
            $update = $BDD->prepare("UPDATE `question` SET `contre` = contre + 1 WHERE id = ?;");
            $update->execute(array($question));
        }else{
            return false;
        }
    }

    public static function auth($email,$password){
        $user = USER::getUser($email);
        $hashpass = hash( "sha512" , $password , false );
        if(!is_null($user) && $hashpass === $user->getPassword()){
            return true;
        }else{
            return false;
        }
    }

    public static function authToken($tokenvote){
        $user = USER::getUserToken($tokenvote);
        if(!is_null($user) && $tokenvote === $user->getTmphash()){
            return true;
        }else{
            return false;
        }
    }

    public static function getUserToken($token){
        $BDD = BDD::getInstance();
        $auth = $BDD->prepare("SELECT * FROM `user` WHERE tmphash = ?");
        $auth->execute(array(strtolower($token)));
        $auth->setFetchMode(PDO::FETCH_CLASS, 'User');
        $res=$auth->fetchall();

        return $res[0];
    }

    public static function getUser($email){
        $BDD = BDD::getInstance();
        $auth = $BDD->prepare("SELECT * FROM `user` WHERE email = ?");
        $auth->execute(array(strtolower($email)));
        $auth->setFetchMode(PDO::FETCH_CLASS, 'User');
        $res=$auth->fetchall();

        return $res[0];
    }

    public static function exist($email){
        $BDD = BDD::getInstance();
        $exist = $BDD->prepare("SELECT count(*) as c FROM `user` WHERE email = ?");
        $exist->execute(array(strtolower($email)));
        $res=$exist->fetchall();

        if($res[0]['c'] > 0){
            return true;
        }else{
            return false;
        }
    }

    public static function existToken($token){
        $BDD = BDD::getInstance();
        $exist = $BDD->prepare("SELECT count(*) as c FROM `user` WHERE tmphash = ?");
        $exist->execute(array($token));
        $res=$exist->fetchall();

        if($res[0]['c'] > 0){
            return true;
        }else{
            return false;
        }
    }

    public static function insertUser($email, $prenom){
        $tmphash = hash( "sha256" , rand(1111111111, 999999999) , false );
        if(USER::exist($email)){
            $BDD = BDD::getInstance();
            $update = $BDD->prepare("UPDATE `user` SET `tmphash` = ? WHERE `user`.`email` = ?;");
            $update->execute(array($tmphash,strtolower($email)));
        }else{
            $BDD = BDD::getInstance();
            $insert = $BDD->prepare("INSERT INTO `user` (`id`, `email`, `prenom`, `password`, `tmphash`) VALUES (NULL, ?, ?, '', ?);");
            $insert->execute(array(strtolower($email), $prenom, $tmphash));
        }
        return true;
    }

    public static function setPassword($token, $password){
        if(USER::existToken($token)){
            $hashpass = hash( "sha512" , $password , false );
            $BDD = BDD::getInstance();
            $update = $BDD->prepare("UPDATE `user` SET `password` = ? WHERE `user`.`tmphash` = ?;");
            $update->execute(array($hashpass, $token));

            USER::delTmpHash($token);
            
            return true;
        }else{
            return false;
        }
    }

    public static function delTmpHash($tmphash){
        $BDD = BDD::getInstance();
        $rsthash = $BDD->prepare("UPDATE `user` SET `tmphash` = ? WHERE `user`.`tmphash` = ?;");
        $rsthash->execute(array("NULL", $tmphash));
        return true;
    }
}