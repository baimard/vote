<?php

class BDD
{
    private static $instance = NULL;

    public static function getInstance() 
    {
        $dsn = "mysql:dbname=".getenv('DBNAME').";host=".getenv('HOST');
        if (!self::$instance) {
            self::$instance = new PDO($dsn, getenv('USER'), getenv('PASSWORD'));
            self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        return self::$instance;
    }

    private function __clone() 
	{
        // interdiction de cloner l'instance
    }

}
