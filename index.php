<?php
    session_start();
    if(time()-$_SESSION["login_time_stamp"] > 7200){
        session_unset();
        session_destroy();
    }
    require "./class/bdd.class.php";
    require "./class/question.class.php";
    require "./class/user.class.php";
    $question=1;
?>

<!doctype html>
<html lang="fr" class="h-100">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="Benjamin AIMARD">

        <title>Votre vote compte</title>    
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
        <link rel="stylesheet" href="style.css">

        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    </head>

    <body class="d-flex h-100 text-white bg-dark">
        <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column text-center">
                    <header class="mb-auto">
                        <div>
                            <h3 class="float-md-start mb-0">Bienvenue <?php echo $_SESSION['prenom'];?></h3>
                            <?php include "./static/menu.php"; ?>
                        </div>
                    </header>
                    <main>
                        <h2><?php echo QUESTION::getQuestion($question)->getLaquestion();?></h2>
                        <?php
                            if (array_key_exists('authenticated', $_SESSION) && $_SESSION['authenticated'] === true) {
                                if(!USER::getUser($_SESSION['email'])->avote()){
                                    include "./static/vote.html";
                                }else{
                                    include './static/share.html';
                                }
                            }else{
                                include "./static/authentification.html";
                            }
                            echo '<div class="p-3 mx-auto flex-column text-center mb-auto" style="display:block"><canvas id="myChart" class="chart"></canvas></div>';
                            echo "<div class='d-none'>Pour : <span id='pour'>" . QUESTION::getQuestion($question)->getPour() . "</span> Contre : <span id='contre'>" . QUESTION::getQuestion($question)->getContre()."</span></div>";
                            echo "<h4>Mise à jour<br/>". ((new DateTime())->format('d-m-Y H:i:s')) . "</h4>";
                        ?>
                        
                    </main>

                    <footer class="mt-auto text-white-50 ">
                        <p><a href="https://www.francetvinfo.fr/sante/maladie/coronavirus/pass-sanitaire-vaccination-obligatoire-isolement-ce-que-contient-la-version-definitive-de-la-loi-sanitaire-adoptee-par-le-parlement_4716431.html">Ce que contient la version définitive de la loi sanitaire adoptée par le Parlement</a></p>
                        <?php include './static/share.html'; ?>
                        <p><Fait par un anonyme, by <a href="https://twitter.com/41m4rd" class="text-white">des anonymes</a> <a href="https://gitlab.com/baimard/vote" class="text-white">source</a> <a class="text-white" href="https://www.buymeacoffee.com/benjaminaimard">Soutenez nous</a></p>
                    </footer>

        </div>
        <?php include "./static/modal.html";?>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.5.0/chart.min.js"></script>
        <script src="js/main.js"></script>
        
    </body>
</html>